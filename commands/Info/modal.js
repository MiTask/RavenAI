

const { ActionRowBuilder, ModalBuilder, TextInputBuilder, TextInputStyle } = require('discord.js');

var unirest = require('unirest');

module.exports = {
    name: "advanced",
    description: "Formatted submit",
    private: false,

    run: async (client, interaction) => {
        // await interaction.deferReply();
      let rand = Math.floor(Math.random() * 99999).toString();

        if (!interaction.isChatInputCommand()) return;

        const modal = new ModalBuilder()
            .setCustomId(rand)
            .setTitle('Submit to the AI');

        // TODO: Add components to modal...

        const promptInput = new TextInputBuilder()
            .setCustomId('sendInput')
            // The label is the prompt the user sees for this input
            .setLabel("Please Provide a Prompt")
            // Short means only a single line of text
            .setStyle(TextInputStyle.Paragraph);



        // An action row only holds one text input,
        // so you need one action row per text input.
        const firstActionRow = new ActionRowBuilder().addComponents([promptInput]);

        // Add inputs to the modal
        modal.addComponents([firstActionRow]);

        await interaction.showModal(modal);

        client.on('interactionCreate', interaction => {

          if (interaction.type !== 5){

          } else {

            if (interaction.customId === rand) {
                (async () => {
                await interaction.reply("Please wait while I think about this...")
                })()

            // Get the data entered by the user
            const data = interaction.fields.getTextInputValue('sendInput');

            function detectCodingLanguages(str) {
                const languages = {
                  'javascript': 'js',
                  'nodejs': 'js',
                  'node': 'js',
                  'python': 'py',
                  'ruby': 'rb',
                  'c#': 'csharp',
                  'c++': 'cpp',
                  'php': 'php',
                  'go': 'go',
                  'bash': 'bash'
          
                };
          
                let detectedLanguages = [];
          
                for (let language in languages) {
                  if (str.includes(language)) {
                    detectedLanguages.push(languages[language]);
                  }
                }
          
                return detectedLanguages;
              }
          
              console.log(detectCodingLanguages(data)[0])
          
              if (detectCodingLanguages(data)) {
                lang = detectCodingLanguages(data)[0]
              } else {
                lang = "js"
              }
          
              unirest
                .post('https://codex-ai-v9q6.onrender.com/')
                .headers({ 'Accept': 'application/json', 'Content-Type': 'application/json' })
                .send({ "prompt": data })
                .then((response) => {
                  if (response.body.bot.length > 1980) {
                    fs.writeFile('/tmp/paste', response.body.bot, err => {
                      if (err) {
                        console.error(err)
                        return
                      }
                    })
                    cmd("sleep 2; cat /tmp/paste | dpaste").then(pasteout => {
                      const mainEmbed = new EmbedBuilder()
                        .setColor('#0099ff')
                        .addFields(
                          { name: 'Please check the below output log:', value: pasteout.stdout.replace("Pro tip: you can password protect your paste just by typing a username and password after your paste command.", "").replace("Paste Saved: ", "").replace("-------------------------------------------------------", "") },
                        ).setTitle("Response too large")
                        .setDescription("Our AI was too Powerful!")
                        .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
                      (async () => {

                        return await interaction.editReply({ embeds: [mainEmbed] })
          
                      })();
          
                    })
                  } else {
                    (async () => {

                    await interaction.editReply("```" + lang + response.body.bot + "```")
                  })();
          
                  }
                })

          }
        }
        });
    }
};