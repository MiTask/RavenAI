const { EmbedBuilder } = require('discord.js');
var unirest = require('unirest');
const cmd = require('cmd-promise')
const fs = require("fs");
let lang
module.exports = {
  name: "send",
  private: false,
  description: "send an inquiry to the AI",
  options: [{
    "name": "inquiry",
    "description": "What you would like to send.",
    "required": true,
    "type": 3 // 6 is type USER
  }],
  run: async (client, interaction) => {
          //  await interaction.deferReply();

    let data = interaction.options._hoistedOptions[0].value

    function detectCodingLanguages(str) {
      const languages = {
        'javascript': 'js',
        'nodejs': 'js',
        'node': 'js',
        'python': 'py',
        'ruby': 'rb',
        'c#': 'csharp',
        'c++': 'cpp',
        'php': 'php',
        'go': 'go',
        'bash': 'bash'

      };

      let detectedLanguages = [];

      for (let language in languages) {
        if (str.includes(language)) {
          detectedLanguages.push(languages[language]);
        }
      }

      return detectedLanguages;
    }

    console.log(detectCodingLanguages(data)[0])

    if (detectCodingLanguages(data)) {
      lang = detectCodingLanguages(data)[0]
    } else {
      lang = "js"
    }

    unirest
      .post('https://codex-ai-v9q6.onrender.com/')
      .headers({ 'Accept': 'application/json', 'Content-Type': 'application/json' })
      .send({ "prompt": data })
      .then((response) => {
        if (response.body.bot.length > 1980) {
          fs.writeFile('/tmp/paste', response.body.bot, err => {
            if (err) {
              console.error(err)
              return
            }
          })
          cmd("sleep 2; cat /tmp/paste | dpaste").then(pasteout => {
            const mainEmbed = new EmbedBuilder()
              .setColor('#0099ff')
              .addFields(
                { name: 'Please check the below output log:', value: pasteout.stdout.replace("Pro tip: you can password protect your paste just by typing a username and password after your paste command.", "").replace("Paste Saved: ", "").replace("-------------------------------------------------------", "") },
              ).setTitle("Response too large")
              .setDescription("Our AI was too Powerful!")
              .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
            (async () => {
              return await interaction.editReply({ embeds: [mainEmbed] })

            })();

          })
        } else {
          (async () => {

          await interaction.editReply("```" + lang + response.body.bot.replaceAll("\`\`\`", "\"\"\"") + "```")
        })();

        }
      })

  },
};
